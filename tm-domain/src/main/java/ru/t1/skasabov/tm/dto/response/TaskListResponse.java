package ru.t1.skasabov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListResponse extends AbstractResponse {

    @NotNull
    private List<TaskDTO> tasks = new ArrayList<>();

    public TaskListResponse(@NotNull final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
