package ru.t1.skasabov.tm.exception.field;

public final class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {
        super("Error! Status is incorrect...");
    }

}
