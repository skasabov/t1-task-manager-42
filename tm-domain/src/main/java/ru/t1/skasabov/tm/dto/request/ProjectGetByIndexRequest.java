package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectGetByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectGetByIndexRequest(@Nullable final Integer index) {
        this.index = index;
    }

}
