package ru.t1.skasabov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public final class Task extends AbstractUserOwnedModel {

    @NotNull
    @Column(nullable = false)
    private String name;

    @Column
    @Nullable
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    private Project project;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    @Nullable
    @Column(name = "begin_date")
    private Date dateBegin;

    @Nullable
    @Column(name = "end_date")
    private Date dateEnd;

}
