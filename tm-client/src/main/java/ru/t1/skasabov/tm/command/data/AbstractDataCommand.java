package ru.t1.skasabov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.skasabov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
