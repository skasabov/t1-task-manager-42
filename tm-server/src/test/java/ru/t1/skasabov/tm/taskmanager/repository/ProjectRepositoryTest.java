package ru.t1.skasabov.tm.taskmanager.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.repository.IProjectRepository;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.*;

public class ProjectRepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    protected SqlSession sqlSession;

    @Before
    @SneakyThrows
    public void initRepository() {
        sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        projectRepository = sqlSession.getMapper(IProjectRepository.class);
        projectRepository.removeAll();
        @NotNull final UserDTO userOne = new UserDTO();
        userOne.setLogin("user_one");
        userOne.setPasswordHash("user_one");
        @NotNull final UserDTO userTwo = new UserDTO();
        userTwo.setLogin("user_two");
        userTwo.setPasswordHash("user_two");
        userRepository.add(userOne);
        userRepository.add(userTwo);
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            if (i <= 5) project.setUserId(USER_ID_ONE);
            else project.setUserId(USER_ID_TWO);
            projectRepository.add(project);
        }
    }

    @Test
    public void testUpdate() {
        @NotNull final ProjectDTO project = projectRepository.findAllForUser(USER_ID_TWO).get(0);
        project.setName("Test Project One");
        project.setDescription("Test Description One");
        projectRepository.update(project);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneByIdForUser(USER_ID_TWO, project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(USER_ID_TWO, actualProject.getUserId());
        Assert.assertEquals("Test Project One", actualProject.getName());
        Assert.assertEquals("Test Description One", actualProject.getDescription());
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClearAll() {
        projectRepository.removeAll();
        Assert.assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final int expectedNumberOfEntries = projectRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        projectRepository.removeAllForUser(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<ProjectDTO> projectList = projectRepository.findAll();
        Assert.assertEquals(projectList.size(), projectRepository.getSize());
    }

    @Test
    public void testFindAllWithNameComparator() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByName();
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        actualProjects.add(1, actualProjects.get(NUMBER_OF_ENTRIES - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithCreatedComparator() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByCreated();
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithStatusComparator() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByStatus();
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            if (i < 4) project.setStatus(Status.COMPLETED);
            else if (i < 7) project.setStatus(Status.IN_PROGRESS);
            else project.setStatus(Status.NOT_STARTED);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<ProjectDTO> projectList = projectRepository.findAllForUser(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectList.size());
    }

    @Test
    public void testFindAllWithNameComparatorForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByNameForUser(USER_ID_TWO);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + (i + 5));
            actualProjects.add(project);
        }
        actualProjects.add(0, actualProjects.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithCreatedComparatorForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByCreatedForUser(USER_ID_ONE);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithStatusComparatorForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByStatusForUser(USER_ID_TWO);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + (i + 5));
            if (i < 3) project.setStatus(Status.COMPLETED);
            else if (i < 5) project.setStatus(Status.IN_PROGRESS);
            else project.setStatus(Status.NOT_STARTED);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindById() {
        @NotNull final ProjectDTO project = projectRepository.findAll().get(0);
        @NotNull final String projectId = projectRepository.findAll().get(0).getId();
        @Nullable final ProjectDTO actualProject = projectRepository.findOneById(projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final ProjectDTO project = projectRepository.findAllForUser(USER_ID_ONE).get(0);
        @NotNull final String projectId = projectRepository.findAllForUser(USER_ID_ONE).get(0).getId();
        @Nullable final ProjectDTO actualProject = projectRepository.findOneByIdForUser(USER_ID_ONE, projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneById(id));
    }

    @Test
    public void testFindByIdProjectNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneByIdForUser(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final ProjectDTO project = projectRepository.findAll().get(0);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneByIndex(0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIndexProjectNotFound() {
        projectRepository.removeAll();
        Assert.assertNull(projectRepository.findOneByIndex(1));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final ProjectDTO project = projectRepository.findAllForUser(USER_ID_TWO).get(0);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneByIndexForUser(USER_ID_TWO, 0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIndexForUserProjectNotFound() {
        projectRepository.removeAll();
        Assert.assertNull(projectRepository.findOneByIndexForUser(USER_ID_ONE, 1));
    }

    @Test
    public void testGetSize() {
        final int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_ONE);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.add(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.getSizeForUser(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = projectRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsById(invalidId));
        Assert.assertTrue(projectRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectRepository.findAllForUser(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsByIdForUser(USER_ID_ONE, invalidId));
        Assert.assertTrue(projectRepository.existsByIdForUser(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = projectRepository.getSizeForUser(USER_ID_ONE) - 1;
        @NotNull final ProjectDTO project = projectRepository.findAllForUser(USER_ID_ONE).get(0);
        projectRepository.removeOne(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSizeForUser(USER_ID_ONE));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = projectRepository.getSize() - 1;
        @NotNull final String projectId = projectRepository.findAll().get(0).getId();
        projectRepository.removeOneById(projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = projectRepository.getSizeForUser(USER_ID_ONE) - 1;
        @NotNull final String projectId = projectRepository.findAllForUser(USER_ID_ONE).get(0).getId();
        projectRepository.removeOneByIdForUser(USER_ID_ONE, projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSizeForUser(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = projectRepository.getSize() - 1;
        projectRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIndexProjectNotFound() {
        projectRepository.removeAll();
        projectRepository.removeOneByIndex(0);
        Assert.assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        final int expectedNumberOfEntries = projectRepository.getSizeForUser(USER_ID_TWO) - 1;
        projectRepository.removeOneByIndexForUser(USER_ID_TWO, 1);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSizeForUser(USER_ID_TWO));
    }

    @Test
    public void testRemoveByIndexForUserProjectNotFound() {
        projectRepository.removeAll();
        projectRepository.removeOneByIndexForUser(USER_ID_ONE, 1);
        Assert.assertEquals(0, projectRepository.getSizeForUser(USER_ID_ONE));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        sqlSession.rollback();
        sqlSession.close();
    }

}
