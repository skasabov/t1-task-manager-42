package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDTO add(@Nullable TaskDTO model);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@Nullable Sort sortType);

    @NotNull
    Collection<TaskDTO> addAll(@Nullable Collection<TaskDTO> models);

    @NotNull
    Collection<TaskDTO> set(@Nullable Collection<TaskDTO> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    TaskDTO findOneById(@Nullable String id);

    @Nullable
    TaskDTO findOneByIndex(@Nullable Integer index);

    int getSize();

    @NotNull
    TaskDTO removeOne(@Nullable TaskDTO model);

    @Nullable
    TaskDTO removeOneById(@Nullable String id);

    @Nullable
    TaskDTO removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<TaskDTO> collection);

    void removeAll();

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sortType);

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable
    TaskDTO removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO create(@Nullable String userId,
                   @Nullable String name, @Nullable String description,
                   @Nullable Date dateBegin, @Nullable Date dateEnd
    );

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id, @Nullable String name, @Nullable String description
    );

    @NotNull
    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
