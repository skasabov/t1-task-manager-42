package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO add(@Nullable ProjectDTO model);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@Nullable Sort sortType);

    @NotNull
    Collection<ProjectDTO> addAll(@Nullable Collection<ProjectDTO> models);

    @NotNull
    Collection<ProjectDTO> set(@Nullable Collection<ProjectDTO> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    ProjectDTO findOneById(@Nullable String id);

    @Nullable
    ProjectDTO findOneByIndex(@Nullable Integer index);

    int getSize();

    @NotNull
    ProjectDTO removeOne(@Nullable ProjectDTO model);

    @Nullable
    ProjectDTO removeOneById(@Nullable String id);

    @Nullable
    ProjectDTO removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<ProjectDTO> collection);

    void removeAll();

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sortType);

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable
    ProjectDTO removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name, @Nullable String description,
            @Nullable Date dateBegin, @Nullable Date dateEnd
    );

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id, @Nullable String name, @Nullable String description
    );

    @NotNull
    ProjectDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
