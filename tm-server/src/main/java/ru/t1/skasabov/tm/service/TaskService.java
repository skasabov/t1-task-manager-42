package ru.t1.skasabov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.ITaskRepository;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.ITaskService;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.TaskNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.dto.model.TaskDTO;

import java.util.*;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<TaskDTO> collection) {
        if (collection == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            for (@NotNull final TaskDTO task : collection) {
                repository.removeOne(task);
            }
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO add(@Nullable final TaskDTO model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(model);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<TaskDTO> addAll(@Nullable final Collection<TaskDTO> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            for (@NotNull final TaskDTO model : models) {
                repository.add(model);
            }
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<TaskDTO> set(@Nullable final Collection<TaskDTO> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAll();
            for (@NotNull final TaskDTO model : models) {
                repository.add(model);
            }
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NotNull List<TaskDTO> tasks;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAll();
        }
        return tasks;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final Sort sortType) {
        if (sortType == null) return findAll();
        @NotNull List<TaskDTO> tasks;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            switch(sortType) {
                case BY_CREATED:
                    tasks = repository.findAllSortByCreated();
                    break;
                case BY_STATUS:
                    tasks = repository.findAllSortByStatus();
                    break;
                case BY_NAME:
                    tasks = repository.findAllSortByName();
                    break;
                default:
                    tasks = Collections.emptyList();
                    break;
            }
        }
        return tasks;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO task;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneById(id);
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable TaskDTO task;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIndex(index);
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO removeOne(@Nullable final TaskDTO model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeOne(model);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public TaskDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO model = findOneById(id);
        if (model == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeOneById(id);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final TaskDTO model = findOneByIndex(index);
        if (model == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeOneByIndex(index);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAll();
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        int size;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            size = repository.getSize();
        }
        return size;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsTask;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            existsTask = repository.existsById(id);
        }
        return existsTask;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAllForUser(userId);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsTask;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            existsTask = repository.existsByIdForUser(userId, id);
        }
        return existsTask;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<TaskDTO> tasks;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAllForUser(userId);
        }
        return tasks;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sortType) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sortType == null) return findAll(userId);
        @NotNull List<TaskDTO> tasks;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            switch(sortType) {
                case BY_CREATED:
                    tasks = repository.findAllSortByCreatedForUser(userId);
                    break;
                case BY_STATUS:
                    tasks = repository.findAllSortByStatusForUser(userId);
                    break;
                case BY_NAME:
                    tasks = repository.findAllSortByNameForUser(userId);
                    break;
                default:
                    tasks = Collections.emptyList();
                    break;
            }
        }
        return tasks;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO task;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIdForUser(userId, id);
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable TaskDTO task;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIndexForUser(userId, index);
        }
        return task;
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        int size;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            size = repository.getSizeForUser(userId);
        }
        return size;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO model = findOneById(userId, id);
        if (model == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeOneByIdForUser(userId, id);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO model = findOneByIndex(userId, index);
        if (model == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeOneByIndexForUser(userId, index);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        if (dateBegin == null || dateEnd == null) return create(userId, name, description);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(task);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(task);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(task);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(task);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull List<TaskDTO> tasks;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAllByProjectId(userId, projectId);
        }
        return tasks;
    }

}
