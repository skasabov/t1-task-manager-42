package ru.t1.skasabov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    Boolean existsByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<ProjectDTO> findAllForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<ProjectDTO> findAllSortByNameForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<ProjectDTO> findAllSortByStatusForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<ProjectDTO> findAllSortByCreatedForUser(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    ProjectDTO findOneByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    ProjectDTO findOneByIndexForUser(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId}")
    int getSizeForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeOneByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_project WHERE id IN " +
            "(SELECT id FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void removeOneByIndexForUser(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void removeAllForUser(@NotNull String userId);

    @Insert("INSERT INTO tm_project (id, name, description, created, user_id, status, begin_date, end_date) " +
            "VALUES (#{id}, #{name}, #{description}, #{created}, #{userId}, #{status}, " +
            "#{dateBegin}, #{dateEnd})")
    void add(@NotNull ProjectDTO model);

    @NotNull
    @Select("SELECT * FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<ProjectDTO> findAll();

    @NotNull
    @Select("SELECT * FROM tm_project ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<ProjectDTO> findAllSortByName();

    @NotNull
    @Select("SELECT * FROM tm_project ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<ProjectDTO> findAllSortByCreated();

    @NotNull
    @Select("SELECT * FROM tm_project ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<ProjectDTO> findAllSortByStatus();

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM tm_project WHERE id = #{id}")
    Boolean existsById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    ProjectDTO findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    ProjectDTO findOneByIndex(@NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_project")
    int getSize();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeOne(@NotNull ProjectDTO model);

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void removeOneById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_project WHERE id IN " +
            "(SELECT id FROM tm_project LIMIT 1 OFFSET #{index})")
    void removeOneByIndex(@NotNull @Param("index") Integer index);

    @Delete("DELETE FROM tm_project")
    void removeAll();

    @Update("UPDATE tm_project SET name = #{name}, description = #{description}, status = #{status} " +
            "where user_id = #{userId} AND id = #{id}")
    void update(@NotNull ProjectDTO project);

}
