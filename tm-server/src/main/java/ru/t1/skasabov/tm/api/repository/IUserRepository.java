package ru.t1.skasabov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password, email, first_name, last_name, middle_name, role, locked) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, " +
            "#{role}, #{locked})")
    void add(@NotNull UserDTO model);

    @NotNull
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    List<UserDTO> findAll();

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM tm_user WHERE id = #{id}")
    Boolean existsById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_user LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneByIndex(@NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_user")
    int getSize();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOne(@NotNull UserDTO model);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOneById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_user WHERE id IN (SELECT id FROM tm_user LIMIT 1 OFFSET #{index})")
    void removeOneByIndex(@NotNull @Param("index") Integer index);

    @Delete("DELETE FROM tm_user")
    void removeAll();

    @Update("UPDATE tm_user SET login = #{login}, password = #{passwordHash}, email = #{email}, " +
            "first_name = #{firstName}, last_name = #{lastName}, middle_name = #{middleName}, " +
            "role = #{role}, locked = #{locked} WHERE id = #{id}")
    void update(@NotNull UserDTO user);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findByEmail(@NotNull @Param("email") String email);

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM tm_user WHERE login = #{login}")
    Boolean isLoginExist(@NotNull @Param("login") String login);

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM tm_user WHERE email = #{email}")
    Boolean isEmailExist(@NotNull @Param("email") String email);

}
