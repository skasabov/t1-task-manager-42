package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    SessionDTO add(@Nullable SessionDTO model);

    @NotNull
    Collection<SessionDTO> addAll(@Nullable Collection<SessionDTO> models);

    @NotNull
    Collection<SessionDTO> set(@Nullable Collection<SessionDTO> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    SessionDTO findOneById(@Nullable String id);

    @Nullable
    SessionDTO findOneByIndex(@Nullable Integer index);

    int getSize();

    @NotNull
    SessionDTO removeOne(@Nullable SessionDTO model);

    @Nullable
    SessionDTO removeOneById(@Nullable String id);

    @Nullable
    SessionDTO removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<SessionDTO> collection);

    void removeAll();

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<SessionDTO> findAll(@Nullable String userId);

    @Nullable
    SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    SessionDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable
    SessionDTO removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    SessionDTO removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

}
