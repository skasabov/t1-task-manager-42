package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    UserDTO add(@Nullable UserDTO model);

    @NotNull
    List<UserDTO> findAll();

    @NotNull
    Collection<UserDTO> addAll(@Nullable Collection<UserDTO> models);

    @NotNull
    Collection<UserDTO> set(@Nullable Collection<UserDTO> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findOneByIndex(@Nullable Integer index);

    int getSize();

    @NotNull
    UserDTO removeOne(@Nullable UserDTO model);

    @Nullable
    UserDTO removeOneById(@Nullable String id);

    @Nullable
    UserDTO removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<UserDTO> collection);

    void removeAll();

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(
            @Nullable String id, @Nullable String firstName,
            @Nullable String lastName, @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login);

}
